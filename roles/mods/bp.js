module.exports = {
    id: 'bp',
    name: 'Ultimate Bullet-proof',
    description: 'You are immune to any bullet kills at night (Mafia, Vigilante, etc).',
    mod: {
        bulletproof: true,
    },
};
