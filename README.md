# MafiaBot

A Discord bot for playing mafia. Customized to a Danganronpa theme + custom features. Original by foolmoron @ GitHub: https://github.com/foolmoron/MafiaBot

## Install

```sh
$ npm install
```

## Config

Setup all the required values in `config.js`

## Run

```sh
$ node --harmony_rest_parameters mafia.js
```
The app uses the Rest Parameters feature so make sure that flag is set. The app will crash immediately if it's not set, so it should be easy to catch.

## Debug

Install [Node Inspector](https://github.com/node-inspector/node-inspector), then

```sh
$ node-debug --nodejs --harmony_rest_parameters mafia-debug.js
```